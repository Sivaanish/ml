# -*- coding: utf-8 -*-
"""Market Basket Analysis.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1hT9l8Wr3O5YjSFT1VOcfaG4SVcaNpPLI
"""

import subprocess
import sys

try:
  import pandas as pd
except ImportError:
  subprocess.check_call([sys.executable,"-m","pip","install",'pandas'])
finally:
  import pandas as pd

import numpy as np  
  
  
# for visualizations
import matplotlib.pyplot as plt
import squarify
import seaborn as sns
plt.style.use('fivethirtyeight')
# for market basket analysis
from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules

data = pd.read_csv('https://bitbucket.org/Sivaanish/ml/downloads/Market_Basket_Optimisation.csv')
print(data.shape)
data.head()

data.tail()

data.sample(20)#getting any 20 randoms values in dataset

print(data.sample(20))
#Visualization data
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

plt.rcParams['figure.figsize'] = (18, 7)
color = plt.cm.copper(np.linspace(0, 1, 40))
data.iloc[0].value_counts().head(40).plot.bar(color = color)
plt.title('frequency of most popular items', fontsize = 20)
plt.xticks(rotation = 90 )
plt.grid()
plt.show()
plt.savefig("Data.png")

trans = []
for i in range(0, 7500):
    trans.append([str(data.values[i,j]) for j in range(0, 20)])

# conveting it into an numpy array
trans = np.array(trans)

# checking the shape of the array
print(trans.shape)

from mlxtend.preprocessing import TransactionEncoder

te = TransactionEncoder()
data = te.fit_transform(trans)
data = pd.DataFrame(data, columns = te.columns_)

# getting the shape of the data
data.shape

print(data.head())


import warnings
warnings.filterwarnings('ignore')

# this is used to get only the items the user wants to check relationship about

data = data.loc[:, ['mineral water', 'burgers', 'turkey', 'chocolate', 'frozen vegetables', 'spaghetti',
                    'shrimp', 'grated cheese', 'eggs', 'cookies', 'french fries', 'herb & pepper', 'ground beef',
                    'tomatoes', 'milk', 'escalope', 'fresh tuna', 'red wine', 'ham', 'cake', 'green tea',
                    'whole wheat pasta', 'pancakes', 'soup', 'muffins', 'energy bar', 'olive oil', 'champagne', 
                    'avocado', 'pepper', 'butter', 'parmesan cheese', 'whole wheat rice', 'low fat yogurt', 
                    'chicken', 'vegetables mix', 'pickles', 'meatballs', 'frozen smoothie', 'yogurt cake']]
print(data.shape)

print(data.columns)#the updated column vlues

print(data.sample(10))

items=apriori(data, min_support = 0.01, use_colnames = True)
print(items)

items['length'] = items['itemsets'].apply(lambda x: len(x))
print(items)

items[ (items['length'] == 3) &
                   (items['support'] >= 0.01) ]

rules = association_rules(items, metric="lift", min_threshold=1)
print(rules.head(100))

rules.rename(columns={'antecedents':'Item A','consequents':'Item B','antecedent support':'Item A support','consequent support':'Item B support'},inplace=True)
print(rules.sample(20))

rules["Item A size"] = rules["Item A"].apply(lambda x: len(x))
print(rules.head())

print(rules[ (rules['Item A size'] >= 2) &
       (rules['confidence'] > 0.075) &
       (rules['lift'] > 1.2) ])